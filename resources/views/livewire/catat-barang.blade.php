<div class="form-row control-group">
    <div class="col">
        <select class=" form-control selectpicker" data-live-search="true">
            <option></option>
            <option selected>Codeigniter</option>
            <option value="FuelPHP">FuelPHP</option>
            <option value="PhalconPHP">PhalconPHP</option>
            <option value="Slim">Slim</option>
            <option value="Silex">Silex</option>
            <option value="CakePHP">CakePHP</option>
            <option value="Symfony">Symfony</option>
            <option value="Yii">Yii</option>
            <option value="Laravel">Laravel</option>
            <option value="Lumen">Lumen</option>
            <option value="Zend">Zend</option>
        </select>
    </div>
    <div class="col-1">
        <input type="text" class="form-control-plaintext text-center" placeholder="id" readonly>
    </div>
    <div class="col-2">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <button class="input-group-text">+</button>
            </div>
            <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
            <div class="input-group-append">
                <button class="input-group-text">-</button>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Rp.</span>
            </div>
            <input type="text" class="form-control" placeholder="Harga Barang" aria-label="Username"
                aria-describedby="basic-addon1" wire:model="harga_satuan.0">
            @error('harga_satuan.0') <span class="text-danger error">{{ $message }}</span>@enderror
        </div>
    </div>
    <div class="col-1">
        <button class="btn btn-primary mb-3" wire:click.prevent="add({{ $i }})"><i
                class="bi bi-plus">+</i></button>
    </div>
    {{-- Add Form --}}
    @foreach ($inputs as $key => $value)
        <div class="form-row control-group">
            <div class="col">
                <select class=" form-control selectpicker" data-live-search="true">
                    <option></option>
                    <option selected>Codeigniter</option>
                    <option value="FuelPHP">FuelPHP</option>
                    <option value="PhalconPHP">PhalconPHP</option>
                    <option value="Slim">Slim</option>
                    <option value="Silex">Silex</option>
                    <option value="CakePHP">CakePHP</option>
                    <option value="Symfony">Symfony</option>
                    <option value="Yii">Yii</option>
                    <option value="Laravel">Laravel</option>
                    <option value="Lumen">Lumen</option>
                    <option value="Zend">Zend</option>
                </select>
            </div>
            <div class="col-1">
                <input type="text" class="form-control-plaintext text-center" placeholder="id" readonly>
            </div>
            <div class="col">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">+</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
                    <div class="input-group-append">
                        <span class="input-group-text">-</span>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                    </div>
                    <input type="text" class="form-control" placeholder="Harga Barang" aria-label="Username"
                        aria-describedby="basic-addon1" wire:model="harga_satuan.{{ $value }}">
                    @error('hargas_satuan.') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="col-1">
                <button class="btn btn-light mb-3 mr-2" wire:click.prevent="remove({{ $key }})"><i
                        class="bi bi-x">-</i></button>
            </div>
        </div>
    @endforeach
    </form>
</div>
@if (session()->has('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
