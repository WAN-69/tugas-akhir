@extends('sb-admin.layout')
@section('judul', 'Halaman Home')
@section('content')
    <div class="card text-center">
        <div class="card-header">
            <ul class="nav nav-pills card-header-pills">
                <li class="nav-item">
                    <h5 class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Mencatat transaksi pembelian
                        barang</h5>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <form id="kotak">
                <div class="form-row d-flex justify-content-end">
                    <div class="btn btn-success btn-sm mt-n3 mb-1 mr-2">Print</div>
                    <div class="btn btn-success btn-sm mt-n3 mb-1 mr-2" wire:click.prevent="store()">Simpan</div>
                </div>
                <hr>
                <div class="form-row text-left  mb-2">
                    <h6 class="col ml-1">Nama Barang</h6>
                    <h6 class="col-1 mr-3">ID</h6>
                    <h6 class="col ">Kuantitas</h6>
                    <h6 class="col">Subtotal</h6>
                </div>
                @livewire('catat-barang')
            </form>
        </div>
    </div>
@endsection
@push('dropdown-script')
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
@endpush
