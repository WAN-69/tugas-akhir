@extends('sb-admin.layout')
@section('judul', 'Halaman Master Barang')
@section('content')
    <a href="/posts/create" class="btn btn-primary mb-3">Tambah</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Harga Barang</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($barang as $key=>$value)
                <tr>
                    <td>{{ $key + 1 }}</th>
                    <td>{{ $value->nama_barang }}</td>
                    <td>{{ $value->harga_satuan }}</td>
                    <td>
                        <a href="/posts/{{ $value->id }}" class="btn btn-info">Show</a>
                        <a href="/posts/{{ $value->id }}/edit" class="btn btn-primary">Edit</a>
                        <form action="/posts/{{ $value->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
