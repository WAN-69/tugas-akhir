<div class="copy invisible">
    <div class="form-row control-group">
        <div class="col">
            <select class=" form-control theSelect" wire:model="nama_barang.0">
                <option></option>
                <option selected>Codeigniter</option>
                <option value="FuelPHP">FuelPHP</option>
                <option value="PhalconPHP">PhalconPHP</option>
                <option value="Slim">Slim</option>
                <option value="Silex">Silex</option>
                <option value="CakePHP">CakePHP</option>
                <option value="Symfony">Symfony</option>
                <option value="Yii">Yii</option>
                <option value="Laravel">Laravel</option>
                <option value="Lumen">Lumen</option>
                <option value="Zend">Zend</option>
            </select>
            @error('account.0') <span class="text-danger error">{{ $message }}</span>@enderror
        </div>
        <div class="col-1 ">
            <input type="text" class="form-control-plaintext text-center" placeholder="id" readonly
                wire:model="harga_satuan.0">
            @error('harga_satuan.0') <span class="text-danger error">{{ $message }}</span>@enderror
        </div>
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Qty</span>
                </div>
                <input type="number" min="1" value="1" class="form-control" placeholder="..." aria-label="Username"
                    aria-describedby="basic-addon1">
            </div>
        </div>
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Rp.</span>
                </div>
                <input type="text" class="form-control" placeholder="Harga Barang" aria-label="Username"
                    aria-describedby="basic-addon1">
            </div>
        </div>
        <div class="col-1">
            <button class="btn btn-danger " type="button"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
</div>
