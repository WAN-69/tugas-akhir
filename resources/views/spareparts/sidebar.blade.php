<div class="">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
            </div>
            <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <li class="nav-item">
            <a class="nav-link active" href="/">
                <i class="fa fa-home" aria-hidden="true"></i>
                <span>Home</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/list">
                <i class="fa fa-credit-card" aria-hidden="true"></i>
                <span>List Transaksi</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="users">
                <i class="fa fa-users" aria-hidden="true"></i>
                <span>Data Pengguna</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="master-barang">
                <i class="fa fa-truck" aria-hidden="true"></i>
                <span class="">Data Master Produk</span>
            </a>
        </li>

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="py-lg-5 text-center ">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
</div>
