<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Http\Request;
use App\Models\master_barang;

class CatatBarang extends Component
{
    public $barangs, $nama_barang, $harga_satuan, $barang_id;
    public $updateMode = false;
    public $inputs = [];
    public $i = 1;

    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs, $i);
    }

    public function remove($i)
    {
        unset($this->inputs[$i]);
    }

    private function resetInputFields()
    {
        $this->nama_barang = '';
        $this->harga_satuan = '';
    }

    public function store()
    {
        $validatedDate = $this->validate(
            [
                'nama_barang.0' => 'required',
                'harga_satuan.0' => 'required',
                'nama_barang.*' => 'required',
                'harga_satuan.*' => 'required',
            ],
            [
                'nama_barang.0.required' => 'nama_barang field is required',
                'harga_satuan.0.required' => 'harga_satuan field is required',
                'nama_barang.*.required' => 'nama_barang field is required',
                'harga_satuan.*.required' => 'harga_satuan field is required',
            ]
        );

        foreach ($this->nama_barang as $key => $value) {
            master_barang::create(['nama_barang' => $this->nama_barang[$key], 'harga_satuan' => $this->harga_satuan[$key]]);
        }

        $this->inputs = [];

        $this->resetInputFields();

        session()->flash('message', 'Account Added Successfully.');
    }

    public function render()
    {
        $data = master_barang::all();
        return view('livewire.catat-barang', ['data' => $data]);
    }
}
